﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Questionary.Utils;

namespace Questionary.Entitys
{
    [Table("tblQuestions")]
    public class Question
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string QuestionText { get; set; }

        [Required]
        public int OrderNum { get; set; }

        [Required]
        public Enums.FormFieldType FormFieldType { get; set; }
    }

    [Table("tblUsersDataFilds")]
    public class UserDataField
    {
        [Key]
        public int Id { get; set; }

        [Required, MaxLength(250)]
        public string FieldName { get; set; }
    }

    [Table("tblQuestionUserDataFieldMap")]
    public class QuestionUserDataFieldMap
    {
        [Key]
        public int Id { get; set; }

        public int QuestionFK { get; set; }

        [ForeignKey("QuestionFK")]
        public Question Question { get; set; }

        public int UserDataFieldFK { get; set; }

        [ForeignKey("UserDataFieldFK")]
        public UserDataField UserDataField { get; set; }
    }

    [Table("tblUsersData")]
    public class UserData
    {
        [Key]
        public int Id { get; set; }

        [Required, MaxLength(16)]
        public string Name { get; set; }

        [Required]
        public int Age { get; set; }

        [Required]
        public Enums.Gender PersonGender { get; set; }

        [Required]
        public DateTime Birthday { get; set; }

        [Required]
        public Enums.MaritalStatus PersonMaritalStatus { get; set; }

        [Required]
        public bool IsLoveProgramming { get; set; }
    }
}
