﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Questionary.BLL;
using Questionary.Models;
using Questionary.Repositories;

namespace Questionary.Controllers
{
    public class HomeController : Controller
    {
        private readonly IQuestionService _service;

        public HomeController(IQuestionService service)
        {
            _service = service;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult QuestionaryForm()
        {
            return View();
        }

        public IActionResult GetQuestionsList()
        {
            var result = _service.GetQuestionsList();
            return Json(
                new
                {
                    isOk = true,
                });
        }
    }
}
