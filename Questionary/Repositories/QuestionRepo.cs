﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Questionary.Domain;
using Questionary.Entitys;

namespace Questionary.Repositories
{

    public interface IQuestionRepo
    {
        //IQueryable<Question> ListAll();
        IList<Question> ListAll(); 
    }

    public class QuestionRepo: IQuestionRepo
    {
        private readonly ApplicationContext _dbContext;

        public QuestionRepo(ApplicationContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IList<Question> ListAll()
        {
            try
            {
                //return _dbContext.Set<Question>().AsNoTracking().AsQueryable();
                //return _dbContext.tblQuestions.AsNoTracking().AsQueryable();
                return _dbContext.tblQuestions.ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

     
    }
}
