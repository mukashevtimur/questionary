﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Questionary.Utils
{
    public class Enums
    {
        public enum Gender
        {
            Male=1,
            Female=2
        }

        public enum MaritalStatus
        {
            Single = 1,
            Married = 2,
            Divorced = 3,
            Widowed = 4
        }

        public enum FormFieldType
        {
            Txt = 1,
            Ddl = 2,
            Calender = 3,
            CheckBox = 4,
            Number = 5
        }
    }
}
