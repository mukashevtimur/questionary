﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Questionary.Entitys;

namespace Questionary.Domain
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {

        }

        public DbSet<Question> tblQuestions { get; set; }
        public DbSet<UserData> tblUsersData { get; set; }
        public DbSet<UserDataField> tblUsersDataFilds { get; set; }
        public DbSet<QuestionUserDataFieldMap> tblQuestionUserDataFieldMap { get; set; }
    }
}
