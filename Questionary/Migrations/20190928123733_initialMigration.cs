﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Questionary.Migrations
{
    public partial class initialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tblQuestions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    QuestionText = table.Column<string>(nullable: false),
                    OrderNum = table.Column<int>(nullable: false),
                    FormFieldType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblQuestions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tblUsersData",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 16, nullable: false),
                    Age = table.Column<int>(nullable: false),
                    PersonGender = table.Column<int>(nullable: false),
                    Birthday = table.Column<DateTime>(nullable: false),
                    PersonMaritalStatus = table.Column<int>(nullable: false),
                    IsLoveProgramming = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblUsersData", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tblUsersDataFilds",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FieldName = table.Column<string>(maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblUsersDataFilds", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tblQuestionUserDataFieldMap",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    QuestionFK = table.Column<int>(nullable: false),
                    UserDataFieldFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblQuestionUserDataFieldMap", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tblQuestionUserDataFieldMap_tblQuestions_QuestionFK",
                        column: x => x.QuestionFK,
                        principalTable: "tblQuestions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_tblQuestionUserDataFieldMap_tblUsersDataFilds_UserDataFieldFK",
                        column: x => x.UserDataFieldFK,
                        principalTable: "tblUsersDataFilds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_tblQuestionUserDataFieldMap_QuestionFK",
                table: "tblQuestionUserDataFieldMap",
                column: "QuestionFK");

            migrationBuilder.CreateIndex(
                name: "IX_tblQuestionUserDataFieldMap_UserDataFieldFK",
                table: "tblQuestionUserDataFieldMap",
                column: "UserDataFieldFK");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tblQuestionUserDataFieldMap");

            migrationBuilder.DropTable(
                name: "tblUsersData");

            migrationBuilder.DropTable(
                name: "tblQuestions");

            migrationBuilder.DropTable(
                name: "tblUsersDataFilds");
        }
    }
}
