﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Questionary.Entitys;
using Questionary.Repositories;
using Questionary.Utils;

namespace Questionary.BLL
{
    public interface IQuestionService
    {
        List<Question> GetQuestionsList();
    }

    public class QuestionService: IQuestionService
    {
        private readonly IQuestionRepo _repo;

        public QuestionService(IQuestionRepo repo)
        {
            _repo = repo;
        }
        public List<Question> GetQuestionsList()
        {
            var questionsList = _repo.ListAll().ToList();



            return questionsList;
        }
        
    }
}
